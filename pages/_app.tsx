/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import React, { Fragment } from "react";
import { Provider } from "react-redux";
import type { AppProps } from "next/app";
// store
import { store } from "apps/shared/core/redux/store";
// types
import { Page } from "apps/shared/types";
// styles
import "styles/globals.scss";

type Props = AppProps & {
  Component: Page<{}>;
};

function MyApp({ Component, pageProps }: Props) {
  const Layout = Component.layout || Fragment;

  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
