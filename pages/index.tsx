import React from "react";
// componets
import Movies from "apps/movie/components/Movies";
import HeaderLayout from "apps/shared/components/HeaderLayout";

const Component = (): JSX.Element => <Movies />;

Component.layout = HeaderLayout;

export default Component;
