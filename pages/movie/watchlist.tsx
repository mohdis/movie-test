import React from "react";
// components
import HeaderLayout from "apps/shared/components/HeaderLayout";
import WatchList from "apps/movie/components/WatchList";

const Component = () => <WatchList />;

Component.layout = HeaderLayout;

export default Component;
