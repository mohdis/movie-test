import React from "react";
// components
import Actor from "apps/actor/components/Actor";
import HeaderLayout from "apps/shared/components/HeaderLayout";

const Component = (): JSX.Element => <Actor />;

Component.layout = HeaderLayout;

export default Component;
