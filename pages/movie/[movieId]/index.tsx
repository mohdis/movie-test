import React from "react";
// components
import Movie from "apps/movie/components/MovieDetail";
import HeaderLayout from "apps/shared/components/HeaderLayout";

const Component = (): JSX.Element => <Movie />;

Component.layout = HeaderLayout;

export default Component;
