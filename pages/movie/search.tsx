import React from "react";
// components
import MoviesSearch from "apps/movie/components/MoviesSearch";
import HeaderLayout from "apps/shared/components/HeaderLayout";

const Component = () => <MoviesSearch />;

Component.layout = HeaderLayout;

export default Component;
