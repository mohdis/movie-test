import React, { useEffect } from "react";
import { useRouter } from "next/router";
// api
import { useWatchListQuery } from "apps/movie/core/redux/movieApiSlice";
// component
import MoviesList from "apps/movie/components/MoviesList";
// helpers
import { getSession } from "apps/shared/core/modules/sessionManager";

const WatchList = (): JSX.Element => {
  const sessionId = getSession();
  const router = useRouter();

  const { data, isLoading } = useWatchListQuery(sessionId);

  useEffect(() => {
    if (sessionId) return;
    router.push("/");
  }, []);

  return (
    <MoviesList movies={data?.results} isLoading={isLoading} isWatchList />
  );
};

export default WatchList;
