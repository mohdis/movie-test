import React, { MouseEvent } from "react";
import Link from "next/link";
// api
import { useAddToWatchListMutation } from "apps/movie/core/redux/movieApiSlice";
// components
import Loading from "apps/shared/components/Loading";
// types
import { MoviesListComponentType, MovieType } from "apps/movie/types";
// helper
import { getSession } from "apps/shared/core/modules/sessionManager";
// constants
import { baseImageUrl } from "apps/shared/core/constants";

const MoviesList = ({
  movies,
  isLoading,
  isWatchList,
}: MoviesListComponentType): JSX.Element => {
  const sessionId = getSession();

  const [removeFromWatchList] = useAddToWatchListMutation();

  const handleClickRemoveFromWatchList = (
    event: MouseEvent<HTMLButtonElement>,
    movie: Partial<MovieType>
  ) => {
    event.stopPropagation();
    removeFromWatchList({ movie, sessionId, watchlist: false });
  };

  if (isLoading) return <Loading />;

  return (
    <div>
      {movies &&
        movies.map((movie) => (
          <Link key={movie.id} href={`/movie/${movie.id}`}>
            <div>
              <img
                alt={movie.title}
                src={`${baseImageUrl}${movie.poster_path}`}
              />
              <p>{movie.title}</p>
              <p>{movie.vote_average}</p>
              {isWatchList && (
                <button
                  type="button"
                  onClick={(e) => handleClickRemoveFromWatchList(e, movie)}
                >
                  Remove from WatchList
                </button>
              )}
            </div>
          </Link>
        ))}
    </div>
  );
};

export default MoviesList;
