import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";
// api
import {
  useMovieQuery,
  useMovieCreditsQuery,
  useAddToWatchListMutation,
} from "apps/movie/core/redux/movieApiSlice";
// component
import Loading from "apps/shared/components/Loading";
// types
import { CastComponentType } from "apps/movie/types";
// helper
import { getSession } from "apps/shared/core/modules/sessionManager";
// constants
import { baseImageUrl } from "@/apps/shared/core/constants";

const Movie = (): JSX.Element => {
  const sessionId = getSession();
  const router = useRouter();
  const movieId = router.query.movieId as string;

  const [addFromWatchList] = useAddToWatchListMutation();
  const { data: movie, isLoading } = useMovieQuery(movieId);
  const { data: credits, isLoading: isLoadingCredits } =
    useMovieCreditsQuery(movieId);

  const handleAddToWatchList = () => {
    addFromWatchList({ sessionId, movie, watchlist: true, });
  };

  if (isLoading) return <Loading />;

  return (
    <div>
      <img alt={movie.title} src={`${baseImageUrl}${movie.poster_path}`} />
      <p>{movie.title}</p>
      <p>{movie.overview}</p>
      <p>{movie.vote_average}</p>
      <p>
        {movie.runtime}
        min
      </p>
      <button type="button" onClick={handleAddToWatchList}>
        Add to WatchList
      </button>
      <div>{isLoadingCredits ? <Loading /> : <Cast cast={credits.cast} />}</div>
    </div>
  );
};

const Cast = ({ cast }: CastComponentType): JSX.Element => {
  const router = useRouter();

  return (
    <div>
      {cast
        .filter((_actor, index) => index < 5)
        .map((actor) => (
          <Link key={actor.id} href={`${router.asPath}/actor/${actor.id}`}>
            <div>
              <img
                alt={actor.name}
                src={`${baseImageUrl}${actor.profile_path}`}
              />
              <p>{actor.name}</p>
              <p>{actor.character}</p>
            </div>
          </Link>
        ))}
    </div>
  );
};

export default Movie;
