import React from "react";
import { useRouter } from "next/router";
// api
import { useMoviesSearchQuery } from "apps/movie/core/redux/movieApiSlice";
// component
import MoviesList from "apps/movie/components/MoviesList";

const MoviesSearch = (): JSX.Element => {
  const router = useRouter();
  const { term } = router.query;

  const { data, isLoading } = useMoviesSearchQuery(term as string);
  return <MoviesList movies={data?.results} isLoading={isLoading} />;
};

export default MoviesSearch;
