import React from "react";
// api
import { useMoviesQuery } from "apps/movie/core/redux/movieApiSlice";
// component
import MoviesList from "apps/movie/components/MoviesList";

const Movies = (): JSX.Element => {
  const { data, isLoading } = useMoviesQuery();

  return <MoviesList movies={data?.results} isLoading={isLoading} />;
};

export default Movies;
