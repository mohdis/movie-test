// api
import { baseApiSlice } from "apps/shared/core/redux/baseApiSlice";
// types
import {
  MovieResponseType,
  MovieType,
  MovieCreditsType,
  AddToWatchListInputType,
  AddToWatchListResponseType,
} from "apps/movie/types";
// constants
import { apiKey } from "apps/shared/core/constants";

export const movieApiSlice = baseApiSlice.injectEndpoints({
  endpoints: (builder) => ({
    movies: builder.query<MovieResponseType, void>({
      query: () => `movie/popular?api_key=${apiKey}`,
    }),
    movie: builder.query<MovieType, string>({
      query: (movieId) => `/movie/${movieId}?api_key=${apiKey}`,
    }),
    movieCredits: builder.query<MovieCreditsType, string>({
      query: (movieId) => `/movie/${movieId}/credits?api_key=${apiKey}`,
    }),
    moviesSearch: builder.query<MovieResponseType, string>({
      query: (term) => `/search/movie?query=${term}&api_key=${apiKey}`,
    }),
    watchList: builder.query<MovieResponseType, string>({
      query: (sessionId) =>
        `/account/1/watchlist/movies?session_id=${sessionId}&api_key=${apiKey}`,
      providesTags: ["WatchList"],
    }),
    addToWatchList: builder.mutation<
      AddToWatchListResponseType,
      AddToWatchListInputType
    >({
      query: ({ sessionId, movie, watchlist }) => ({
        url: `/account/1/watchlist?session_id=${sessionId}&api_key=${apiKey}`,
        method: "POST",
        body: {
          media_type: "movie",
          media_id: movie.id,
          watchlist,
        },
      }),
      // invalidatesTags: ["WatchList"],
      onQueryStarted: async (
        { watchlist, sessionId, movie },
        { dispatch, queryFulfilled }
      ) => {
        const patchResult = dispatch(
          movieApiSlice.util.updateQueryData(
            "watchList",
            sessionId,
            (draft) => {
              if (watchlist) {
                draft.results.push(movie);
              } else {
                const newWatchList = draft.results.filter(
                  (currentMovie: MovieType) => currentMovie.id !== movie.id
                );
                Object.assign(draft, { ...draft, results: newWatchList });
              }
            }
          )
        );

        queryFulfilled.catch(patchResult.undo);
      },
    }),
  }),
  overrideExisting: false,
});

export const {
  useMoviesQuery,
  useMovieQuery,
  useMovieCreditsQuery,
  useMoviesSearchQuery,
  useWatchListQuery,
  useAddToWatchListMutation,
} = movieApiSlice;
