export type MovieType = {
  id: number;
  title: string;
  poster_path: string;
  release_date: string;
  vote_average: number;
  overview: string;
  overview_title: string;
  genres: GenreType[];
  runtime: number;
  budget: number;
  revenue: number;
};

export type GenreType = { id: number; name: string };

export type CastType = {
  id: number;
  name: string;
  character: string;
  order: number;
  profile_path: string;
};

export type MovieCreditsType = {
  id: string;
  cast: CastType[];
};

export type MovieResponseType = {
  page: number;
  total_pages: number;
  results: Partial<MovieType>[];
  total_result: number;
};

export type AddToWatchListInputType = {
  sessionId: string;
  watchlist: boolean;
  movie: Partial<MovieType>;
};

export type AddToWatchListResponseType = {
  status_code: number;
  status_message: number;
};

export type CastComponentType = {
  cast: CastType[];
};

export type MoviesListComponentType = {
  isLoading: boolean;
  isWatchList?: boolean;
  movies: Partial<MovieType>[] | undefined;
};
