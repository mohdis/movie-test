export type ActorType = {
  id: number;
  name: string;
  profile_path: string;
  biography: string;
  popularity: number;
  place_of_birth: string;
  known_for_department: string;
  also_known_as: string[];
};
