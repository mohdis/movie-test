// api
import { baseApiSlice } from "apps/shared/core/redux/baseApiSlice";
// types
import { ActorType } from "apps/actor/types";
// constants
import { apiKey } from "apps/shared/core/constants";

export const actorApi = baseApiSlice.injectEndpoints({
  endpoints: (builder) => ({
    actor: builder.query<ActorType, string>({
      query: (actorId) => `/person/${actorId}?api_key=${apiKey}`,
    }),
  }),
  overrideExisting: false,
});

export const { useActorQuery } = actorApi;
