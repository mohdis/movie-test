import React from "react";
import { useRouter } from "next/router";
import { useActorQuery } from "apps/actor/core/redux/actorApiSlice";
// component
import Loading from "apps/shared/components/Loading";
// contants
import { baseImageUrl } from "@/apps/shared/core/constants";

const Actor = (): JSX.Element => {
  const router = useRouter();
  const { actorId } = router.query;

  const { data: actor, isLoading } = useActorQuery(actorId as string);

  if (isLoading) return <Loading />;

  return (
    <div>
      <img alt={actor.name} src={`${baseImageUrl}${actor.profile_path}`} />
      <p>{actor.name}</p>
      <p>{actor.biography}</p>
      <p>Place of brith: {actor.place_of_birth}</p>
    </div>
  );
};

export default Actor;
