import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useFormik } from "formik";
// types
import { UserCredentialsType } from "apps/signin/types";
// api
import {
  useCreateSessionMutation,
  useCreateTokenMutation,
  useValidateTokenWithLoginMutation,
} from "apps/signin/core/redux/signinApiSlice";
// component
import Loading from "apps/shared/components/Loading";
// helper
import { setSession } from "apps/shared/core/modules/sessionManager";
// hooks
import useIsSignedin from "apps/shared/core/hooks/useIsSignedin";

const initialValues: UserCredentialsType = {
  username: "",
  password: "",
};

const Signin = () => {
  const router = useRouter();
  const [isSignedin] = useIsSignedin();

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [createToken] = useCreateTokenMutation();
  const [validateTokenWithLogin] = useValidateTokenWithLoginMutation();
  const [createSession] = useCreateSessionMutation();

  const redirectToRoot = () => {
    router.push("/");
  };

  const { values, handleChange, handleSubmit } = useFormik({
    initialValues,
    onSubmit: async (submitedValues) => {
      setIsLoading(true);
      createToken()
        .unwrap()
        .then((responseCreateToken) =>
          validateTokenWithLogin({
            ...submitedValues,
            request_token: responseCreateToken.request_token,
          })
            .unwrap()
            .then((responseCreateTokenWithLogin) =>
              createSession(responseCreateTokenWithLogin.request_token)
                .unwrap()
                .then((responseCreateSession) => {
                  setSession(responseCreateSession.session_id);
                  redirectToRoot();
                })
            )
        );
    },
  });

  useEffect(() => {
    if (!isSignedin) return;
    redirectToRoot();
  }, [isSignedin]);

  if (isLoading) return <Loading />;

  return (
    <form onSubmit={handleSubmit}>
      <input name="username" value={values.username} onChange={handleChange} />
      <input
        name="password"
        type="password"
        value={values.password}
        onChange={handleChange}
      />
      <button type="submit">Signin</button>
    </form>
  );
};

export default Signin;
