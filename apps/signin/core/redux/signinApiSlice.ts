// api
import { baseApiSlice } from "apps/shared/core/redux/baseApiSlice";
// types
import {
  CreateTokenResponseType,
  ValidateTokenWithLoginInputType,
  CreateSessionResponseType,
} from "apps/signin/types";
// constants
import { apiKey } from "apps/shared/core/constants";

export const signinApi = baseApiSlice.injectEndpoints({
  endpoints: (builder) => ({
    createToken: builder.mutation<CreateTokenResponseType, void>({
      query: () => `authentication/token/new?api_key=${apiKey}`,
    }),
    validateTokenWithLogin: builder.mutation<
      CreateTokenResponseType,
      ValidateTokenWithLoginInputType
    >({
      query: (data) => ({
        url: `authentication/token/validate_with_login?api_key=${apiKey}`,
        method: "POST",
        body: data,
      }),
    }),
    createSession: builder.mutation<CreateSessionResponseType, string>({
      query: (requestToken) => ({
        url: `authentication/session/new?api_key=${apiKey}`,
        method: "POST",
        body: { request_token: requestToken },
      }),
    }),
    deleteSession: builder.mutation<void, string>({
      query: (sessionId) => ({
        url: `authentication/session?api_key=${apiKey}`,
        method: "DELETE",
        body: { session_id: sessionId },
      }),
    }),
  }),
  overrideExisting: false,
});

export const {
  useCreateTokenMutation,
  useValidateTokenWithLoginMutation,
  useCreateSessionMutation,
  useDeleteSessionMutation,
} = signinApi;
