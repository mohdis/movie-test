export type CreateTokenResponseType = {
  request_token: string;
  success: boolean;
};

export type CreateSessionResponseType = {
  session_id: string;
  success: boolean;
};

export type ValidateTokenWithLoginInputType = UserCredentialsType & {
  request_token: string;
};

export type UserCredentialsType = {
  username: string;
  password: string;
};
