import React, { ChangeEvent, useState, useCallback } from "react";
import debounce from "lodash/debounce";
import { useRouter } from "next/router";
import Link from "next/link";
// types
import { HeaderLayoutComponentType } from "apps/shared/types";
// api
import { useDeleteSessionMutation } from "apps/signin/core/redux/signinApiSlice";
// helper
import {
  deleteSession as deleteSessionOnCookies,
  getSession,
} from "apps/shared/core/modules/sessionManager";
// hooks
import useIsSignedin from "apps/shared/core/hooks/useIsSignedin";

const HeaderLayout = ({ children }: HeaderLayoutComponentType): JSX.Element => {
  const router = useRouter();

  const [searchValue, setSearchValue] = useState<string>("");
  const [_, refresh] = useState<object>();

  const [isSignedin] = useIsSignedin();
  const [deleteSession] = useDeleteSessionMutation();

  const pushToSearch = (term: string) => {
    if (!term) {
      router.push("/");
      return;
    }

    router.push({ pathname: "/movie/search", query: { term } });
  };

  const deboundedPushToSearch: (term: string) => void = useCallback(
    debounce(pushToSearch, 1000),
    []
  );

  const handleChangeSearchInput = (e: ChangeEvent<HTMLInputElement>) => {
    const term = e.target.value;

    setSearchValue(term);
    deboundedPushToSearch(term);
  };

  const handleLogout = () => {
    deleteSession(getSession())
      .unwrap()
      .then(() => {
        deleteSessionOnCookies();
        refresh({});
      });
  };

  return (
    <>
      <div>
        {isSignedin ? (
          <>
            <button type="button" onClick={handleLogout}>
              Logout
            </button>
            <Link href="/movie/watchlist">Go to Watchlist</Link>
          </>
        ) : (
          <Link href="/signin">
            <button type="button">Signin</button>
          </Link>
        )}
        <input
          placeholder="search movies"
          value={searchValue}
          onChange={handleChangeSearchInput}
        />
      </div>
      {children}
    </>
  );
};

export default HeaderLayout;
