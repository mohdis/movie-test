import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
// constants
import { baseUrl } from "apps/shared/core/constants";

export const baseApiSlice = createApi({
  tagTypes: ["WatchList"],
  reducerPath: "baseApiSlice",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers) => {
      headers.set("Content-Type", "application/json;charset=utf-8");

      return headers;
    },
  }),
  endpoints: () => ({}),
});
