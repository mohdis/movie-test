import Cookies from "universal-cookie";

const key = "session_id";

const cookies = new Cookies();

export const setSession = (sessionId: string) => {
  cookies.set(key, sessionId);
};

export const deleteSession = () => {
  cookies.remove(key);
};

export const getSession = (): string | undefined => {
  const sessionId = cookies.get(key) as string | undefined;
  return sessionId;
};
