import { useState, useEffect } from "react";
// helpers
import { getSession } from "apps/shared/core/modules/sessionManager";

const useIsSignedin = (): [boolean, string | undefined] => {
  const [sessionId, setSessionId] = useState<undefined | string>(undefined);
  const session = getSession();

  useEffect(() => setSessionId(session), [session]);

  return [!!sessionId, sessionId];
};

export default useIsSignedin;
