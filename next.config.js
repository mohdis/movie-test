/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["image.tmdb.org"],
  },
  async redirects() {
    return [
      {
        source: "/movie",
        destination: "/",
        permanent: true,
      },
      {
        source: "/movie/:movieId/actor",
        destination: "/movie/:movieId",
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
